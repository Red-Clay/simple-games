
# Dada la situacion en este commit: es tedioso estructurar el frontend.

## Juegos HTML-Simples

- Interfaz de botones en fila (4 Juegos)
- 1 html, 2 js, 1 css

## Juegos:

- 1: Ordenar Numeros (Asc,Desc)
- 2: Adivinar Numero entre 1-100
- 3: Apuesta dinero con un dado de 6 caras
- 4: Ahorcado

## Tareas

- [x] Uso de css y html estatico
- [x] Comprobacion de UX simple
- [x] Uso de una clase para utilizar los metodos correspondientes a cada juego
- [x] Uso simple de JQuery para crear otros elementos
- [x] Control de errores y finalizaciones de los juegos
- [x] Reseteo de Juegos

# Mejoras y Errores

### Errores

- [ ] Reiteraciones de funciones
- [ ] Confusion entre los nombres de funciones
### Mejoras
- [ ] Cambios apresurados debido al desconocimiento frontend
- [ ] Poca escalibilidad de juegos


# favicon:
- Freepik
