class Juegos {
  constructor(hangman_states,hangman_palabras) {
    // BOTONES
    this.HangStatus = hangman_states
    this.HangState =  0
    this.HangWords = hangman_palabras
    this.CorrectLetters = ""
    this.LettersUsed = []
    this.LoopedLetters = ""
    this.WinHangman = false
    
    this.numeros = ['0','1', '2','3','4','5','6','7','8','9',"."]
    this.DiceNumbers = ['&#9856','&#9857','&#9858','&#9859','&#9860','&#9861']
    this.NumeroDado = 0
    this.GamesActions = {
      // JUEGO1 -> BotoneFuncion1: DescripcionBoton2 ,...
      "OrdenarNumeros": 
          {"OrdenAscendente":"Ordenar Ascendentemente","OrdenDescendente":"Ordenar Descendentemente"},
      "AdivinarNumeros": 
          {"ComprobarRandom":"CheckNumber","NewRandom":"NuevoRandom"},

      "CasinoDados": 
          {"LuckyNumber":"Apuesta!"},

      "Ahorcado":
          {"ComprobarLetra":"Comprobar la letra","CambiarPalabra":"Cambiar palabra"},
    }



    this.Input= document.getElementById("GameInput");
    this.Message = document.getElementById("MessageValue");
    this.Hangman = document.getElementById("hangman");

    this.GameBox= document.getElementById("Game-output-input");
    this.GameName = document.getElementById("GameName");
    this.OutputTitle= document.getElementById("OutputTitle");

    this.RandomNumber = 0
    this.CasinoMoney = 100
    this.DiceResult = 1

    this.NewWord = ""
    // this.ChosenGame = document.getElementsByClassName("ChangeMyName") // Nombre ambiguo
    

    this.GameID= ""

    this.HideElement(this.GameBox) //OCULTAR OTRA VEZ

    this.AllButtons= document.getElementById("AllButtons");

    // TECLADO
    /* document.addEventListener('keydown', e => this.keyboardInputHandler(e));     */ 
  }

  CreateButtons(fnname,btnname){
    $(document).ready(function() {
        // Your jQuery code here
       $("#Input-box").append("<button class='GameElements' type='button' onclick='Games." + fnname + "()'>" + btnname + "</button>")
    });
  }

  CreateInputs(id_name,type,event){
    $(document).ready(function() {
       $("#Input-box").append("<input type='" + type + "' id='" + id_name + "' class='GameElements' " + event + "  >")
    });
  }
  
  CreateElements(id_name,tag){
    $(document).ready(function() {
       $("#Input-box").append("<" + tag + " id='" + id_name + "' class='GameElements' >0€</" + tag + ">")});
  }

  
  UpdateRange(val){
    document.getElementById('DineroApostado').textContent = val + "€"
  }
  

  HideElement(elemento){
    elemento.style.display = "none";
  }
  ShowElement(elemento){ 
    elemento.style.display = "grid";
  }

  StartGame(){
    this.HideElement(this.AllButtons)
    this.ShowElement(this.GameBox)
    this.Message.innerHTML = ""
    this.ShowElement(this.Input)
  }

  Indice(){
    this.Input.value = ""
    this.CorrectLetters = ""
    this.LettersUsed = []
    this.LoopedLetters = ""
    this.HangState
        // Your jQuery code here
    $( ".GameElements" ).remove();
    this.Intentos = 10
    this.Message.innerHTML = ""
    this.HideElement(this.GameBox)
    this.ShowElement(this.AllButtons)
  }

  GenerarRandom(limit){
    return Math.floor((Math.random() * limit) + 1);
  }

  SetGame(title,game_id){
    this.Input.removeAttribute("maxlength"); 
    this.GameName.innerText = title
    document.title = title
    this.GameID = game_id

    //Comprobacion de Juego
    let id_games = Object.keys(this.GamesActions)
    if (!id_games.includes(game_id)){
      alert("No coincide IDGAME: " + game_id)
      return
    }

    //Crear los btones con las funciones correspondientes y sus Descripciones
    let subbotones = Object.keys(this.GamesActions[game_id])
    subbotones.forEach( id_func => {
      let btnname = this.GamesActions[game_id][id_func]
      this.CreateButtons(id_func,btnname)
    });

    
     this.StartGame() // Orden de los factores si afecta a Javascript


    switch (game_id) {
      case "OrdenarNumeros":
        
        this.OutputTitle.innerHTML = 'Numeros Ordenados';
        this.Input.setAttribute("placeholder", "Ejemplo [-∞,∞] : 5,4,3,2,1...");
        break;
      case "AdivinarNumeros":
        this.HideElement(this.Input)
        this.Intentos = 10;
        this.RandomNumber = this.GenerarRandom(100) 
        this.OutputTitle.innerHTML = 'Numero aleatorio desconocido: ' + "¿?";
        //alert("Este es tu numero random: " + this.RandomNumber.toString())
        /* this.Input.setAttribute("placeholder", "Ejemplo [1-100] : 23"); */
        this.CreateInputs("GameInput","range"," min='1' value='1' oninput='Games.AdivinarValor()'");

        break;
      case "CasinoDados":

        this.OutputTitle.innerHTML = 'Dinero : ' + this.CasinoMoney.toString() +"€";
        this.Input.setAttribute("placeholder", "Ejemplo [1-6] : 2 ");
        this.CreateInputs("NumeroDado","number"," min=\"1\" max=\"6\" value=\"1\" oninput='Games.ModificarValores()' ")
        $(document).ready(function() {
          $("[type='number']").keypress(function (evt) {
            evt.preventDefault(); 
          });
        })
        this.CreateInputs("DineroApostado","range"," min='1' max='100' value='1' oninput='Games.ModificarValores()'");

        this.HideElement(this.Input)
        setTimeout(() => this.ModificarValores(), 50);

        break;
      case "Ahorcado":
        this.NewWord = this.GenerarPalabra()
        this.Input.addEventListener("keypress", function(event) {
        // If the user presses the "Enter" key on the keyboard
        if (event.key === "Enter") {
        // Cancel the default action, if needed
        event.preventDefault();
        // Trigger the button element with a click
        document.getElementsByClassName("GameElements")[0].click();
        }
        });
        //this.OutputTitle.innerHTML = this.NewWord; DEBUG 
        this.Input.setAttribute("placeholder", "Ejemplo [Aa-Zz] : B");
        this.Input.setAttribute("maxlength", "1");
        this.HiddenWord()
        this.ActualizarHangman()
        break;
      default:
        break;
    }

  }
  
    AdivinarValor(){
    // const AciertoNumero = 
    // AciertoNumero.setAttribute("max","100")
    this.NumeroDado = document.getElementsByClassName('GameElements')[2].value;
    if (this.NumeroDado == "" || this.NumeroDado== " ") {
      this.NumeroDado = "1"
    }
    this.OutputTitle.innerHTML = 'Numero aleatorio desconocido: ' + "¿" + this.NumeroDado + "?";

  }

    ///////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////// Funciones de los juegos ///////////////////////// 
    ///////////////////////////////////////////////////////////////////////////////////////////
  OrdenAscendente(){
    if (this.Input.value == "") {
      return
    }
    let str_numeros = this.Input.value; 
    let lista_numbers = str_numeros.split(","); 
    lista_numbers = lista_numbers.map(function (x) {return parseFloat(x); });// str -> int
    lista_numbers.sort(function(a, b){return a-b}); 
    this.Message.innerHTML= lista_numbers.join(",")
  }

  OrdenDescendente(){
    if (this.Input.value == "") {
      return
    }

    let str_numeros = this.Input.value;
    let lista_numbers = str_numeros.split(","); 
    lista_numbers = lista_numbers.map(function (x) {return parseFloat(x); }); // str -> int 
    lista_numbers.sort(function(a, b){return b-a}); 
    this.Message.innerHTML= lista_numbers.join(",");
  }

    ///////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////// RANDOM NUMBER/////////////////////////////////////////////

  ///////////////////////////////////////////////////////////////////////////////////////////


  ComprobarRandom(){
    let user_num = this.NumeroDado;
    user_num = parseInt(user_num,10);
    if (this.Intentos <= 0){
      this.Message.innerHTML= "Numero Secreto: " + this.RandomNumber + "Se Acabo el numero de intentos reinicia la pagina";
      return;
    }
    this.Intentos = this.Intentos - 1;
    let n_tries = this.Intentos.toString()
    if (user_num > this.RandomNumber){
      this.Message.innerHTML= "Demasiado Alto - Intentos: " + n_tries;
      document.getElementsByClassName("GameElements")[2].setAttribute("max", this.NumeroDado )
      this.Message.style.color = "red";
    }else if (user_num < this.RandomNumber){
      this.Message.innerHTML= "Demasiado Bajo - Intentos: " + n_tries;
      document.getElementsByClassName("GameElements")[2].setAttribute("min", this.NumeroDado )
      this.Message.style.color = "red";
    }else{
      this.Message.innerHTML= "Adivinaste el numero " + this.RandomNumber + "- Intentos: " + n_tries;
      this.Message.style.color = "green";
    }
  }

  ModificarValores(){

    const DineroApostado = document.getElementById('DineroApostado');
    DineroApostado.setAttribute("max",this.CasinoMoney.toString())
    const NumeroDado = document.getElementById('NumeroDado');
    if (NumeroDado.value == "" || NumeroDado.value == " ") {
      NumeroDado.value = "1"
    }
    this.OutputTitle.innerHTML = "Dinero Apostado:" + DineroApostado.value + "€ - Dinero Actual:" + this.CasinoMoney.toString() + " € - Tu dado:" + NumeroDado.value + "<span style=\"font-size: 40px;\"> " + this.DiceNumbers[NumeroDado.value - 1] + "</span>";

  }

  NewRandom(){
        this.Intentos = 10;
        this.RandomNumber = this.GenerarRandom(100) 
        this.OutputTitle.innerHTML = 'Numero aleatorio desconocido: ';
          document.getElementsByClassName("GameElements")[2].setAttribute("max", "100" )
          document.getElementsByClassName("GameElements")[2].setAttribute("min", "1" )
  }
  
  GenerarDado(repetitions){
      // Reiteracion? // Javascript es asincrono por "naturaleza"
    //
      let n_random = this.GenerarRandom(6) - 1
      let random_dice = this.DiceNumbers[n_random]
      this.Message.innerHTML = "<span style=\"font-size: 80px;\"> " + random_dice + "</span>";
    if (repetitions > 0) {
        setTimeout(() => this.GenerarDado(repetitions - 1), 50);
    }else{
      this.DiceResult = n_random;
    }
    
  }

  ComprobarResultado(){

    const NumeroDado = document.getElementById('NumeroDado');
    // DiceResult es un array
    if (this.DiceResult + 1 == parseInt(NumeroDado.value,10) ){
      this.CasinoMoney += parseInt(DineroApostado.value,10)
      this.Message.innerHTML += "<br> <p style=\"color:green\">+" + DineroApostado.value + "€</p>" 
      this.ModificarValores()
    }else {
      this.CasinoMoney -= parseInt(DineroApostado.value,10)
      this.Message.innerHTML += "<br>  <p style=\"color:red\"> -" + DineroApostado.value + "€</p>"
      this.ModificarValores()
      if (this.CasinoMoney <= 0) {
        this.OutputTitle.innerHTML = "Estas en negativo!"
        document.getElementsByClassName("GameElements")[0].disabled = true
        document.getElementsByClassName("GameElements")[1].disabled = true
        document.getElementsByClassName("GameElements")[2].disabled = true
      }
    }

  }

  LuckyNumber(){
    // Randomizar y mostrar
    
    this.GenerarDado(50)
    setTimeout(() => this.ComprobarResultado(), 3000);//3 segundos
  }

  // JUEGO 3
  
   ActualizarHangman(){
    this.Input.value = ""
    let lastTries = this.HangStatus.length - this.HangState - 1
    this.OutputTitle.innerHTML = "Letras Usadas: " + this.LettersUsed.join(",") + " <br>Intentos: " + lastTries + "<br>Palabra: " + this.LoopedLetters;
    if (this.WinHangman){
      document.getElementsByClassName("GameElements")[0].disabled = true
      this.OutputTitle.innerHTML += "<br> GANASTE !" 
    }
    if(lastTries <= 0){
      document.getElementsByClassName("GameElements")[0].disabled = true
      this.OutputTitle.innerHTML = "Letras Usadas: " + this.LettersUsed.join(",") + " <br>Intentos: " + lastTries + "<br>Palabra: " + this.NewWord
      this.OutputTitle.innerHTML += "<br> PERDISTE !" 
    }
  this.Input.focus()
  }
  
  ComprobarLetra(){
    if (this.Input.value == "" || this.Input.value == " " ){
      return
    }
    // MINUSCULAS
    this.Input.value = this.Input.value.toLowerCase()

    if (!this.LettersUsed.includes(this.Input.value)){
      this.LettersUsed.push(this.Input.value)     
    }else{
      return
    }

    if(this.NewWord.includes(this.Input.value)){
      this.CorrectLetters += this.Input.value
      this.LoopedLetters = ""
      this.HiddenWord()

    }else if(this.HangState < this.HangStatus.length - 1){
      this.HangState += 1
      this.HiddenWord()

    }

    let assci_picture=this.HangStatus[this.HangState]
    this.Message.innerHTML = "<pre>" + assci_picture +"</pre>";
    this.ActualizarHangman()

  }

  GenerarPalabra(){
    let limit = this.HangWords.length
    let a_word = Math.floor((Math.random() * limit));
    return this.HangWords[a_word]
  }

  HiddenWord(){
    let LCorrects = 0
    this.LoopedLetters = ""
    for (let i = 0; i < this.NewWord.length; i++) {
      const Letter = this.NewWord[i];
      if (this.CorrectLetters.includes(Letter)){
        this.LoopedLetters += " <b><u>" + Letter + "</u></b> "
        LCorrects++
        if (LCorrects == this.NewWord.length){
          this.WinHangman = true
        }
        // Set game ending
      } else {
        this.LoopedLetters += " <b><u>_</u></b> "
      }
    }
  }

  CambiarPalabra(){
    this.NewWord = this.GenerarPalabra()
    //this.OutputTitle.innerHTML = this.NewWord; DEBUG 
    document.getElementsByClassName("GameElements")[0].disabled = false

    this.Input.value = ""
    this.Message.innerHTML = ""


    this.WinHangman = false
    this.HangState =  0
    this.CorrectLetters = ""
    this.LettersUsed = []
    this.LoopedLetters = ""
    this.NewWord = this.GenerarPalabra()
    this.OutputTitle.innerHTML = this.NewWord; 
    this.HiddenWord()
    this.ActualizarHangman()
  }
}



let Games = new Juegos(HangmanStates,HangmanWords);








