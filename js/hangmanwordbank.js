
var HangmanStates = ["",`
  +---+
  |   |
      |
      |
      |
      |
=========`, `
  +---+
  |   |
  O   |
      |
      |
      |
=========`, `
  +---+
  |   |
  O   |
  |   |
      |
      |
=========`, `
  +---+
  |   |
  O   |
 /|   |
      |
      |
=========`, `
  +---+
  |   |
  O   |
 /|\\  |
      |
      |
=========`, `
  +---+
  |   |
  O   |
 /|\\  |
 /    |
      |
=========`, `
  +---+
  |   |
  O   |
 /|\\  |
 / \\  |
      |
=========`]

var HangmanWords = [
  "casa",
  "perro",
  "gato",
  "lago",
  "sol",
  "flor",
  "rio",
  "mar",
  "arbol",
  "montaña",
  "camino",
  "playa",
  "libro",
  "tren",
  "coche",
  "avion",
  "barco",
  "pajaro",
  "reloj",
  "silla",
  // "Telefonear",
  // "Adherir",
  // "Reina",
  // "Bofetada",
  // "Salchicha",
  // "Erotismo",
  // "Perfume",
  // "Valle",
  // "Nudo",
  // "Cuchara",
  // "Champán",
  // "Novela",
  // "Novia",
  // "Boxeador",
  // "Loco",
  // "Decorar",
  // "Roto",
  // "Fichas",
  // "Animal",
  // "Combustible",
  // "Concierto",
  // "Madurar",
  // "Fósforo",
  // "Olimpiadas",
  // "Agujas",
  // "Captar",
  // "Internacional",
  // "Gozar",
  // "Pollo",
  // "Secundaria",
  // "Indios",
  // "Disco",
  // "Feo",
  // "Hawaiano",
  // "Esposas",
  // "Coma",
  // "Piernas",
  // "Sabor",
  // "Protesta",
  // "Maquina",
  // "Herida",
  // "Pedestal",
  // "Negocios",
  // "Leche",
  // "Artistas",
  // "Cargar",
  // "Esteroides",
  // "Separar",
  // "Masajista",
  // "España",
  // "Colador",
  // "Amanecer",
  // "Sonrojarse",
  // "Pantano",
  // "Marchitar",
  // "Esperar",
  // "Desgastar",
  // "Meta",
  // "Rosado",
  // "Tetera",
  // "Mejor",
  // "Piedad",
  // "Perro",
  // "Hoja",
  // "Bengala",
  // "Cantar",
  // "Groenlandia",
  // "Clase",
  // "Daga",
  // "Trueno",
  // "Oxidado",
  // "Biombo",
  // "Rata",
  // "Bonito",
  // "Parquímetro",
  // "Desperdiciar",
  // "Elevarse",
  // "Alegría",
  // "Ladrillo",
  // "Latir",
  // "Reloj",
  // "Gelatina",
  // "Cavar",
  // "Bandera",
  // "Verano",
  // "Agenda",
  // "Hambre",
  // "Muchacho",
  // "Granada",
  // "Estufa",
  // "Brazos",
  // "Toser",
  // "Joyas",
  // "Levadura",
  // "Temeroso",
  // "Incendiar",
  // "Llama",

];

